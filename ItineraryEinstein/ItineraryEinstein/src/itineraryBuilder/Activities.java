package itineraryBuilder;
import java.util.*;
import javax.swing.*;
import java.io.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.FlowLayout;
@SuppressWarnings("serial")
public class Activities extends nameGUI implements ActionListener{
	public String strName;
	public String strNumber;
	public Activities(String Name, String Num) throws FileNotFoundException {
		getActivNPrices();
		Name(Name);
		Number(Num);
	}

	private final int MAX = 10;
	private final int ACT = 5;
	public String[] strActivities = new String[MAX];
	public double[] dPrices = new double[MAX];
	public String[] ActChoices = new String[ACT];
	public double[] PriceChoice = new double[ACT];
	public double dTotal;
	public String[] choices = new String[ACT];
	public String[] strWeeks = new String[ACT];
	public String[] strDays = {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday"};
	public String strWeek;
	public String Choice;
	public String[] strTimeList1 = new String[ACT];
	public String[] strTimeList2 = new String[ACT];
	public String[] strTimes = new String[ACT];
	public String[] Choice1 = new String[ACT];
	JTextField txt;
	JTextField txt2;
	JTextField txt3;
	JTextField txt4;
	JButton cmdPrint;
	JButton cmdExit;
	public void getActivNPrices() throws FileNotFoundException {
		String[] Activ = new String[MAX];
		double[] prices = new double[MAX];
		int counter =0;
		File activ = new File("activities.txt");
		@SuppressWarnings("resource")
		Scanner input = new Scanner(activ);
		input.useDelimiter("/");
			while(input.hasNext()) {
				Activ[counter] = input.next();
				prices[counter] = Double.parseDouble(input.next());
				counter++;
			}// while
		strActivities = Activ;
		dPrices = prices;
		Choices();
	}// activities and prices
	public void Choices() throws FileNotFoundException {
		int k = JOptionPane.showConfirmDialog(null,"Please Choose 5 Activities from the next list.","null", JOptionPane.OK_CANCEL_OPTION);
		if(k == JOptionPane.YES_OPTION) {
			String x = "Please Choose Your Desired Activity\n";
			JFrame frame = new JFrame(x);
			for(int i = 0; i < ACT; i++) {
			choices[i] = JOptionPane.showInputDialog(frame,"1.) "+ "Rock Climbing...$34.99"+"\n"+"2.) "+"Hiking Trip...$25.00"+"\n"+"3.) "+"Rafting...$15.99"+"\n"+"4.) "+"Canoeing...$10.99"+"\n"+"5.) "+"Skiing/Snowboarding...$85.00"+"\n"+"6.) "+"Zip Lining...$40.00"+"\n"+"7.) "+"Mountain Biking...$38.00"+"\n"+"8.) "+"Water Park Adventure...$128.89"+"\n"+"9.) "+"ATV Rental...$52.99"+"\n"+"10.) "+"River Fishing Trip...$45.00");
			}
			evaluateChoices(choices, dPrices, strActivities);
		}
		else if(k == JOptionPane.CANCEL_OPTION) {
			System.exit(0);
		}

	}
	public void evaluateChoices(String[] choices, double[] prices, String[] activities) throws FileNotFoundException {		// Evaluates the choices made for activities and sets the activity names and prices accordingly
		String nChoice;
		for(int i = 0; i < ACT; i++) {
			nChoice = choices[i];
			switch(nChoice) {
			case "1":
				ActChoices[i] = strActivities[0];
				PriceChoice[i] = dPrices[0];
				break;
			case "2":
				ActChoices[i] = strActivities[1];
				PriceChoice[i] = dPrices[1];
				break;
			case "3":
				ActChoices[i] = strActivities[2];
				PriceChoice[i] = dPrices[2];
				break;
			case "4":
				ActChoices[i] = strActivities[3];
				PriceChoice[i] = dPrices[3];
				break;
			case "5":
				ActChoices[i] = strActivities[4];
				PriceChoice[i] = dPrices[4];
				break;
			case "6":
				ActChoices[i] = strActivities[5];
				PriceChoice[i] = dPrices[5];
				break;
			case "7":
				ActChoices[i] = strActivities[6];
				PriceChoice[i] = dPrices[6];
				break;
			case "8":
				ActChoices[i] = strActivities[7];
				PriceChoice[i] = dPrices[7];
				break;
			case "9":
				ActChoices[i] = strActivities[8];
				PriceChoice[i] = dPrices[8];
				break;
			case "10":
				ActChoices[i] = strActivities[9];
				PriceChoice[i] = dPrices[9];
				break;

			}//switch
			dTotal += PriceChoice[i];
		}// for

		getWeeksNdays();
	}// end evaluate choices
	public void getWeeksNdays() throws FileNotFoundException {
		String[] dates = new String[5];
		int counter = 0;
		File date = new File("weeks.txt");
		@SuppressWarnings("resource")
		Scanner input = new Scanner(date);
		input.useDelimiter("/");
		while(input.hasNext()) {
			dates[counter] = input.next();
			counter++;
		}// while
		strWeeks = dates;
		vChoices();
	}// days and weeks
	public void vChoices() throws FileNotFoundException {
		int i = JOptionPane.showConfirmDialog(null, "The resort is open for 5 weeks. Please choose 1 from the next window.",null, JOptionPane.OK_CANCEL_OPTION);
		if(i == JOptionPane.YES_OPTION) {
			String x = "Please Choose Your Desired Activity\n";
			JFrame frame = new JFrame(x);
			Choice = JOptionPane.showInputDialog(frame,"1.) "+ "June 3, 2018"+"\n"+"2.) "+"June 10, 2018"+"\n"+"3.) "+"June 17,2018"+"\n"+"4.) "+"July 1, 2018"+"\n"+"5.) "+"July 8, 2018");
			evaluateChoices(Choice, strWeeks);
		}
		else if(i == JOptionPane.CANCEL_OPTION) {
			System.exit(0);
		}
	}
	public void evaluateChoices(String choice, String[] weeks) throws FileNotFoundException {
		switch(choice) {
		case "1":
			strWeek = strWeeks[0];
			break;
		case "2":
			strWeek = strWeeks[1];
			break;
		case "3":
			strWeek = strWeeks[2];
			break;
		case "4":
			strWeek = strWeeks[3];
			break;
		case "5":
			strWeek = strWeeks[4];
			break;
	}
		getTimes();
}
	public void getTimes() throws FileNotFoundException {
		String[] times1 = new String[ACT];
		int counter = 0;
		File time = new File("times1.txt");
		@SuppressWarnings("resource")
		Scanner input = new Scanner(time);
		input.useDelimiter("/");
		while(input.hasNext()) {
			times1[counter] = input.next();
			counter++;
		}// while
		strTimeList1 = times1;
		String[] times2 = new String[ACT];
		int counter2 = 0;
		File time2 = new File("times2.txt");
		@SuppressWarnings("resource")
		Scanner input2 = new Scanner(time2);
		input2.useDelimiter("/");
		while(input2.hasNext()) {
			times2[counter2] = input2.next();
			counter2++;
		}// while
		strTimeList2 = times2;
		Times1();
	}// get times
	public void Times1() {
		String x = "Please Choose Your Desired Time for Monday:  ";
		JFrame frame = new JFrame(x);
		Choice1[0] = JOptionPane.showInputDialog(frame, "Monday Slots [1 or 2]: 10:30am or 3:30pm");
		Times2();
	}
	public void Times2() {
		String x = "Please Choose Your Desired Time for Tuesday: ";
		JFrame frame = new JFrame(x);
		Choice1[1] = JOptionPane.showInputDialog(frame, "Tuesday Slots [1 or 2]: 9:45am or 2:00pm");
		Times3();
	}
	public void Times3() {
		String x = "Please Choose Your Desired Time for Wednesday: ";
		JFrame frame = new JFrame(x);
		Choice1[2] = JOptionPane.showInputDialog(frame, "Wednesday Slots [1 or 2]: 11:00am or 6:00pm");
		Times4();
	}
	public void Times4() {
		String x = "Please Choose Your Desired Time for Thursday: ";
		JFrame frame = new JFrame(x);
		Choice1[3] = JOptionPane.showInputDialog(frame, "Thursday Slots [1 or 2]: 10:00am or 12:30pm");
		Times5();
	}
	public void Times5() {
		String x = "Please Choose Your Desired Time for Friday: ";
		JFrame frame = new JFrame(x);
		Choice1[4] = JOptionPane.showInputDialog(frame, "Friday Slots [1 or 2]: 10:30am or 3:30pm");
		evaluateChoices2(Choice1);
	}
	public void evaluateChoices2(String[] choices) {
		if(Choice1[0] == "1") {
			strTimes[0] = strTimeList1[0];
		}
		else  {
			strTimes[0] = strTimeList2[0];
		}
		if(Choice1[1] == "1") {
			strTimes[1] = strTimeList1[1];
		}
		else  {
			strTimes[1] = strTimeList2[1];
		}
		if(Choice1[2] == "1") {
			strTimes[2] = strTimeList1[2];
		}
		else  {
			strTimes[2] = strTimeList2[2];
		}
		if(Choice1[3] == "1") {
			strTimes[3] = strTimeList1[3];
		}
		else  {
			strTimes[3] = strTimeList2[3];
		}
		if(Choice1[4] == "1") {
			strTimes[4] = strTimeList1[4];
		}
		else {
			strTimes[4] = strTimeList2[4];
		}
		printInfo();
	}
	public void Name(String name) {
		strName = name;
	}
	public void Number(String num) {
		strNumber = num;
	}
	
	public void printInfo() {
		JFrame frame = new JFrame();
		JTextField week = new JTextField("Your Itinerary for the week of: "+ strWeek);
		week.setEditable(false);
		JTextField div = new JTextField("------------------------------------------------------------");
		div.setEditable(false);
		JTextField day1 = new JTextField(strDays[0]+ " :: "+ActChoices[0]+" :: $"+PriceChoice[0]+" :: "+ strTimes[0]);
		day1.setEditable(false);
		JTextField day2 = new JTextField(strDays[1]+ " :: "+ActChoices[1]+" :: $"+PriceChoice[1]+" :: "+ strTimes[1]);
		day2.setEditable(false);
		JTextField day3 = new JTextField(strDays[2]+ " :: "+ActChoices[2]+" :: $"+PriceChoice[2]+" :: "+ strTimes[2]);
		day3.setEditable(false);
		JTextField day4 = new JTextField(strDays[3]+ " :: "+ActChoices[3]+" :: $"+PriceChoice[3]+" :: "+ strTimes[3]);
		day4.setEditable(false);
		JTextField day5 = new JTextField(strDays[4]+ " :: "+ActChoices[4]+" :: $"+PriceChoice[4]+" :: "+ strTimes[4]);
		day5.setEditable(false);
		JTextField div2 = new JTextField("-------------------------------------------------------------");
		div2.setEditable(false);
		JTextField total = new JTextField("Total of trip: $"+dTotal);
		total.setEditable(false);
		cmdPrint = new JButton("Print");
		cmdExit = new JButton("Exit");
		frame.add(week);
		frame.add(div);
		frame.add(day1);
		frame.add(day2);
		frame.add(day3);
		frame.add(day4);
		frame.add(day5);
		frame.add(div2);
		frame.add(total);
		frame.add(cmdPrint);
		frame.add(cmdExit);
		cmdPrint.addActionListener(this);
		cmdExit.addActionListener(this);
		frame.setSize(350,350);
		frame.setLayout(new FlowLayout());
		frame.setTitle("Itinerary");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
	public void actionPerformed(ActionEvent event) {
		switch(event.getActionCommand()) {
		case "Print":
			try {
				PrintWriter output = new PrintWriter("Itinerary.txt");
				output.printf("Itinerary Generated........."+"\n");
				output.printf("Name: "+ strName+"   "+ "Confirmation Number: "+ strNumber+"\n");
				output.printf("----------------------------------------------------------"+"\n");
				output.printf(strDays[0]+ " :: "+ActChoices[0]+" :: $"+PriceChoice[0]+" :: "+ strTimes[0]+"\n");
				output.printf("----------------------------------------------------------"+"\n");
				output.printf(strDays[1]+ " :: "+ActChoices[1]+" :: $"+PriceChoice[1]+" :: "+ strTimes[1]+"\n");
				output.printf("----------------------------------------------------------"+"\n");
				output.printf(strDays[2]+ " :: "+ActChoices[2]+" :: $"+PriceChoice[2]+" :: "+ strTimes[2]+"\n");
				output.printf("----------------------------------------------------------"+"\n");
				output.printf(strDays[3]+ " :: "+ActChoices[3]+" :: $"+PriceChoice[3]+" :: "+ strTimes[3]+"\n");
				output.printf("----------------------------------------------------------"+"\n");
				output.printf(strDays[4]+ " :: "+ActChoices[4]+" :: $"+PriceChoice[4]+" :: "+ strTimes[4]+"\n");
				output.printf("----------------------------------------------------------"+"\n");
				output.printf("Total of trip: $"+dTotal);
				output.close();
			} catch (FileNotFoundException e) {e.printStackTrace();}
			break;
		case "Exit":
			System.exit(0);
		}

}
}// class
