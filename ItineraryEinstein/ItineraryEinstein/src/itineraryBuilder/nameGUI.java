package itineraryBuilder;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class nameGUI extends JFrame implements ActionListener {
	JLabel lblName;
	JLabel lblConfnum;
	JButton cmdStart;
	JButton cmdExit;
	JTextField txtName;
	JTextField txtConfnum;
	String strName;
	String strConfNum;
	public final int WIDTH = 400;
	public final int HEIGHT = 300;
	
	public nameGUI() {
		buildGUI();
	}// constructor
	public void buildGUI() {
		lblName = new JLabel("Name Used For Reservation:");
		lblConfnum = new JLabel("7 Digit Confirmation Number:");
		cmdStart = new JButton("Start");
		cmdExit = new JButton("Exit");
		txtName = new JTextField(30);
		txtConfnum = new JTextField(7);
		add(lblName);
		add(txtName);
		add(lblConfnum);
		add(txtConfnum);
		add(cmdStart);
		add(cmdExit);
		cmdStart.addActionListener(this);
		cmdExit.addActionListener(this);
		setSize(WIDTH, HEIGHT);
		setLayout(new FlowLayout());
		setTitle("Welcome To Itinerary Einstein");
		setLocation(350,350);
		setVisible(true);
	}// gui 1 building
	@SuppressWarnings("deprecation")
	public void actionPerformed(ActionEvent event) {
		switch(event.getActionCommand()) {
		case "Start":
			hide();			// hides window when it moves on
			strName = txtName.getText();
			strConfNum = txtConfnum.getText();
			try {@SuppressWarnings("unused")
			Activities act = new Activities(strName,strConfNum);} catch (FileNotFoundException e) {e.printStackTrace();}
			break;
		case "Exit":
			System.exit(0);
		}
		
	}// action performed

}// class
