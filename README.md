!!!THIS PROGRAM IS STILL IN ITS PROTOTYPE PHASE!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
With that out of the way, I am an IST student for Pennsylvania State University.
This was a project that I wrote for my IST 242 class. It still has many bugs that
I am aware of and have noted. Please be advised that in its current state, the
project cannot be turned into a .jar file. There will be an open issue until
Summer 2018 (because I have class obligations.) that keeps the JFrame from
nameGUI.class open keeping the program from moving on. I also have various
classes written to convert this from files to databases. Those modifications
should be ready by late Summer 2018. The program was written in eclipse but will
run in IntelliJ. There might be an error in the console, but the program works.
I will be adding more as I develop this app.